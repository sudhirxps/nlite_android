package vu.co.sudhirxps.nlite.nlite;

import java.util.List;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by admin7 on 4/11/15.
 */
public interface ApiEndpointInterface {


        // Request method and URL specified in the annotation
        // Callback for the parsed response is the last parameter

        @GET("/xnotice/users/{id}.json")
        Call<User> getUser(@Path("id") String userId);

        @GET("/xnotice/groups/{id}.json")
        Call<Group> getGroup(@Path("id") String groupId);

        @GET("/xnotice/groups/")
        Call<List<Group>> getGroups();

        @GET("/xnotice/notices/")
        Call<List<Notice>> getNotices(@Query("user") String id);

        @POST("/xnotice/register/")
        Call<Register> registerForEvent(@Body Register register);


}
