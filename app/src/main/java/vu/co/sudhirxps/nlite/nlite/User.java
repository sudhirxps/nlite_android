package vu.co.sudhirxps.nlite.nlite;

import com.google.gson.annotations.SerializedName;



import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class User {
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("groups")
    @Expose
    private List<String> groups = new ArrayList<String>();
    @SerializedName("register")
    @Expose
    private List<Object> register = new ArrayList<Object>();

    /**
     *
     * @return
     * The url
     */
    public String getUrl() {
        return url;
    }

    /**
     *
     * @param url
     * The url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     *
     * @return
     * The username
     */
    public String getUsername() {
        return username;
    }

    /**
     *
     * @param username
     * The username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     *
     * @return
     * The email
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     * The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return
     * The groups
     */
    public List<String> getGroups() {
        return groups;
    }

    /**
     *
     * @param groups
     * The groups
     */
    public void setGroups(List<String> groups) {
        this.groups = groups;
    }

    /**
     *
     * @return
     * The register
     */
    public List<Object> getRegister() {
        return register;
    }

    /**
     *
     * @param register
     * The register
     */
    public void setRegister(List<Object> register) {
        this.register = register;
    }


}