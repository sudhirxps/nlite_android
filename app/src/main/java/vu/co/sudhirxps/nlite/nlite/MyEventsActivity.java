package vu.co.sudhirxps.nlite.nlite;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class MyEventsActivity extends AppCompatActivity {


    public static final String BASE_URL = "http://xlite.herokuapp.com";
    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    private ListView list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_events);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        final ApiEndpointInterface apiService =
                retrofit.create(ApiEndpointInterface.class);
        list = (ListView) findViewById(R.id.myEventsListView);

        Call<List<Notice>> noticeCall = apiService.getNotices("http://xlite.herokuapp.com/xnotice/users/1/");
        noticeCall.enqueue(new Callback<List<Notice>>() {
            @Override
            public void onResponse(Response<List<Notice>> response, Retrofit retrofit) {
                int statusCode = response.code();

                // Construct the data source
                ArrayList<Notice> groups = (ArrayList<Notice>) response.body();

                // Create the adapter to convert the array to views
                NoticeAdapter adapter = new NoticeAdapter(getApplicationContext(), groups);
                // Attach the adapter to a ListView

                list.setAdapter(adapter);

            }

            @Override
            public void onFailure(Throwable t) {

            }

        });
    }

}
