package vu.co.sudhirxps.nlite.nlite;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;
import vu.co.sudhirxps.nlite.nlite.Notice;
import vu.co.sudhirxps.nlite.nlite.R;

public class NoticeAdapter extends ArrayAdapter<Notice> {
    public static final String BASE_URL = "http://xlite.herokuapp.com";
    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    private Context context;

    public NoticeAdapter(Context context, ArrayList<Notice> notice) {
        super(context, 0, notice);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {
        // Get the data item for this position
        final Notice notice = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.row, parent, false);
        }
        // Lookup view for data population
        TextView largeText = (TextView) convertView.findViewById(R.id.text1);
        TextView smallText = (TextView) convertView.findViewById(R.id.text2);
        Button button = (Button) convertView.findViewById(R.id.button);
        // Populate the data into the template view using the data object
        largeText.setText(notice.getSubject());
        smallText.setText(notice.getVenue());
        // Return the completed view to render on screen

        if(!notice.getRegistration()){
            button.setVisibility(View.INVISIBLE);
        }else{
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Move to detail view


                    Register registerForEvent = new Register();
                    registerForEvent.setNotice(notice.getUrl());
                    registerForEvent.setUser("http://xlite.herokuapp.com/xnotice/users/1/");
                    new RegisterEventTask().execute(registerForEvent);
                    Snackbar.make(parent, "Registered Successfully ", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();

                }
            });
        }

        largeText.setTextColor(Color.BLACK);
        smallText.setTextColor(Color.RED);
        Typeface roboto = Typeface.createFromAsset(context.getAssets(),
                "font/RobotoCondensed-Light.ttf"); //use this.getAssets if you are calling from an Activity

        largeText.setTypeface(roboto);
        smallText.setTypeface(roboto);

        return convertView;
    }
    private class RegisterEventTask extends AsyncTask<Register, Void, Register> {


        protected Register doInBackground(Register... registers) {
            // Some long-running task like downloading an image.
            final Register registerForEvent = (registers[0]);

            final ApiEndpointInterface apiService =
                    retrofit.create(ApiEndpointInterface.class);

            Call<Register> registerCall = apiService.registerForEvent(registerForEvent);
            registerCall.enqueue(new Callback<Register>() {
                @Override
                public void onResponse(Response<Register> response, Retrofit retrofit) {

                    int statusCode = response.code();
                    Log.d("Retro", ((Register) response.body()).getNotice());

                }

                @Override
                public void onFailure(Throwable t) {
                    t.printStackTrace();
                }
            });
            return registerForEvent;
        }
    }
}