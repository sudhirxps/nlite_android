package vu.co.sudhirxps.nlite.nlite;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.math.BigInteger;


public class Notice {

    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("subject")
    @Expose
    private String subject;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("owner")
    @Expose
    private String owner;
    @SerializedName("department")
    @Expose
    private String department;
    @SerializedName("registration")
    @Expose
    private Boolean registration;
    @SerializedName("deadline")
    @Expose
    private String deadline;
    @SerializedName("contact")
    @Expose
    private String contact;
    @SerializedName("contact_no")
    @Expose
    private BigInteger contactNo;
    @SerializedName("venue")
    @Expose
    private String venue;
    @SerializedName("image")
    @Expose
    private String image;

    /**
     *
     * @return
     * The url
     */
    public String getUrl() {
        return url;
    }

    /**
     *
     * @param url
     * The url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     *
     * @return
     * The subject
     */
    public String getSubject() {
        return subject;
    }

    /**
     *
     * @param subject
     * The subject
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     *
     * @return
     * The description
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     * The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return
     * The owner
     */
    public String getOwner() {
        return owner;
    }

    /**
     *
     * @param owner
     * The owner
     */
    public void setOwner(String owner) {
        this.owner = owner;
    }

    /**
     *
     * @return
     * The department
     */
    public String getDepartment() {
        return department;
    }

    /**
     *
     * @param department
     * The department
     */
    public void setDepartment(String department) {
        this.department = department;
    }

    /**
     *
     * @return
     * The registration
     */
    public Boolean getRegistration() {
        return registration;
    }

    /**
     *
     * @param registration
     * The registration
     */
    public void setRegistration(Boolean registration) {
        this.registration = registration;
    }

    /**
     *
     * @return
     * The deadline
     */
    public String getDeadline() {
        return deadline;
    }

    /**
     *
     * @param deadline
     * The deadline
     */
    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }

    /**
     *
     * @return
     * The contact
     */
    public String getContact() {
        return contact;
    }

    /**
     *
     * @param contact
     * The contact
     */
    public void setContact(String contact) {
        this.contact = contact;
    }

    /**
     *
     * @return
     * The contactNo
     */
    public BigInteger getContactNo() {
        return contactNo;
    }

    /**
     *
     * @param contactNo
     * The contact_no
     */
    public void setContactNo(BigInteger contactNo) {
        this.contactNo = contactNo;
    }

    /**
     *
     * @return
     * The venue
     */
    public String getVenue() {
        return venue;
    }

    /**
     *
     * @param venue
     * The venue
     */
    public void setVenue(String venue) {
        this.venue = venue;
    }

    /**
     *
     * @return
     * The image
     */
    public String getImage() {
        return image;
    }

    /**
     *
     * @param image
     * The image
     */
    public void setImage(String image) {
        this.image = image;
    }

}